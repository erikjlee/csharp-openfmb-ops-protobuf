# C# language protobuf for OpenFMB operational use cases

This repository contains the [C#](https://docs.microsoft.com/en-us/dotnet/csharp/) programming language Protocol Buffer (protobuf) definitions based on the OpenFMB operational use case data model located [here](https://gitlab.com/openfmb/data-models/ops).

## Including in your project

Add an 'openfmb' folder inside of your project, and copy the OpenFMB C# Platform Specific *.cs files into it.

You'll also need to pull the [Google Protocol Buffers](https://www.nuget.org/packages/Google.Protobuf/) Nuget package.
    

## Using

Add the folowing namespaces:

```c++
using Google.Protobuf;
using openfmb.commonmodule;
using openfmb.metermodule;
//TODO: other OpenFMB modules here as needed
```

Deserializing pubsub payload into an OpenFMB Profile instance:

```c++
private MeterReadingProfile deserializeMeterReadingProfile(byte[] payload)
{
    return MeterReadingProfile.Parser.ParseFrom(payload);
}
```

Creating & serializing an OpenFMB profile instance:

```c++
private MeterReadingProfile getMeterReadingProfile()
{
    MeterReadingProfile profile = new MeterReadingProfile();
    profile.Meter = new Meter();
    profile.Meter.ConductingEquipment = new ConductingEquipment();
    profile.Meter.ConductingEquipment.NamedObject = new NamedObject();
    profile.Meter.ConductingEquipment.NamedObject.Name = "name";
    profile.Meter.ConductingEquipment.MRID = Guid.NewGuid().ToString();
    profile.MeterReading = new MeterReading();
    profile.MeterReading.ConductingEquipmentTerminalReading = new ConductingEquipmentTerminalReading();
    profile.MeterReading.ConductingEquipmentTerminalReading.Terminal = new Terminal();
    profile.MeterReading.ConductingEquipmentTerminalReading.Terminal.Phases = new Optional_PhaseCodeKind();
    profile.MeterReading.ConductingEquipmentTerminalReading.Terminal.Phases.Value = PhaseCodeKind.AN;
    profile.MeterReading.PhaseMMTN = new PhaseMMTN();
    profile.MeterReading.PhaseMMTN.PhsA = new ReadingMMTN();
    profile.MeterReading.PhaseMMTN.PhsA.DmdWh = new BCR();
    //profile.MeterReading.PhaseMMTN.PhsA.DmdVAh
    //profile.MeterReading.PhaseMMTN.PhsA.DmdVArh
    profile.MeterReading.PhaseMMTN.PhsA.DmdWh.ActVal = (long)123.45;
    profile.MeterReading.PhaseMMTN.PhsA.DmdWh.T = new Timestamp();
    profile.MeterReading.PhaseMMTN.PhsA.DmdWh.T.Seconds = (ulong)(DateTime.Now - new DateTime(1970, 1, 1)).TotalSeconds;
    return profile;
}

private byte[] serializeProfile(MeterReadingProfile profile)
{
    byte[] payload = null;

    using (var stream = new MemoryStream())
    {
        profile.WriteTo(stream);
        payload = stream.ToArray();
    }

    return payload;
}


```


## Copyright

See the COPYRIGHT file for copyright information of information contained in this repository.

## License

Unless otherwise noted, all files in this repository are distributed under the Apache Version 2.0 license found in the LICENSE file.